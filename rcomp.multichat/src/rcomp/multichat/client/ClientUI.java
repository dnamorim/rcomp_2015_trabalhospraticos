/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rcomp.multichat.client;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author dnamorim
 */
class Variables {

	public static ClientUI chat = null;
	public static Map<Socket, String> serversConnected = new HashMap<>();
	public static Map<String, Thread> threadsList = new HashMap<>();
	public static Map<Socket, Integer> stateServers = new HashMap<>();
	public static Map<String, InetAddress> ipServers = new HashMap<>();
	;
	public static Map<String, Integer> portsServers = new HashMap<>();

	static final String NICKNAME_PREFIX = "NIK";
	static final String DISCONNECT_PREFIX = "OFF";
	static final String CONNECTION_PREFIX = "CON";
	static final String MESSAGE_PREFIX = "MSG";
	static final int TIMEOUT_SOCKET = 3;

	public static final int SEND_RECIEVE = 0;
	public static final int SEND_ONLY = 1;
	public static final int RECIEVE_ONLY = 2;

	public static void startServerSearch() {
		new Thread(new ServerSearchThread(ClientUI.BASE_UDP_PORT)).start();
	}

	public static void disconnect(String nickname) {
		if (!serversConnected.isEmpty()) {
			for (Socket socket : serversConnected.keySet()) {
				try {
					threadsList.get(serversConnected.get(socket)).interrupt();
					Variables.endMessage(socket, nickname);
					socket.close();
				} catch (IOException ex) {
					Logger.getLogger(Variables.class.getName()).
						log(Level.SEVERE, null, ex);
				}
			}

			for (Thread thread : threadsList.values()) {
				thread.interrupt();
			}
		}

		System.exit(0);
	}

	public static boolean setNickname(Socket socket, String nickname) throws IOException {
		DataOutputStream out = new DataOutputStream(socket.getOutputStream());
		DataInputStream in = new DataInputStream(socket.getInputStream());
		out.writeUTF(String.format("%s_%s", NICKNAME_PREFIX, nickname));
		out.flush();
		String message = in.readUTF();
		String msgArr[] = message.split("\\?");
		if (msgArr[0].contains(String.
			format("%s_%s", NICKNAME_PREFIX, "DENIED"))) {
			return false;
		} else {
			if (msgArr[0].contains(String.
				format("%s_%s", NICKNAME_PREFIX, "ACCEPTED"))) {
				return true;
			}

		}
		return false;
	}

	public static void endMessage(Socket socket, String nickname) {
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(socket.getOutputStream());
			out.writeUTF(String.format("%s_%s", DISCONNECT_PREFIX, nickname));
		} catch (IOException ex) {
			Logger.getLogger(Variables.class.getName()).
				log(Level.SEVERE, null, ex);
		} finally {
			try {
				out.close();
			} catch (IOException ex) {
				Logger.getLogger(Variables.class.getName()).
					log(Level.SEVERE, null, ex);
			}
		}

	}

	public static void deleteServer(Socket deleteSocket) {
		if (serversConnected.containsKey(deleteSocket)) {
			threadsList.get(serversConnected.get(deleteSocket)).interrupt();
			threadsList.remove(serversConnected.get(deleteSocket));
			stateServers.remove(deleteSocket);
			serversConnected.remove(deleteSocket);
		}
	}

	public static void disconnectServer(Socket delete, String nickname) {
		deleteServer(delete);
		endMessage(delete, nickname);
	}

	public static Socket connectServer(String name, InetAddress ip,
									   Integer udp_port) {
		if (!serversConnected.containsValue(name)) {
			try {
				Socket socket = new Socket(ip, udp_port + 1);
				return socket;
			} catch (IOException ex) {
				JOptionPane.
					showMessageDialog(chat, "TCP failed.", "Connection Error", JOptionPane.ERROR_MESSAGE);
				return null;
			}
		} else {
			JOptionPane.
				showMessageDialog(chat, "Server already connected.", "Existing Connection", JOptionPane.WARNING_MESSAGE);
			return null;
		}
	}

	public static void connectServers(
		List<InetAddress> availableServers,
		Map<String, InetAddress> ipAddressMap,
		Map<String, Integer> portsMap) {

		for (InetAddress address : availableServers) {
			for (int i = 0; i < 20; i += 2) {
				try {
					DatagramSocket socket = new DatagramSocket();
					socket.setSoTimeout(TIMEOUT_SOCKET);
					socket.setBroadcast(true);
					String msg = CONNECTION_PREFIX;
					byte[] data = msg.getBytes();
					DatagramPacket request = new DatagramPacket(data, msg.
																length(), address, i + ClientUI.BASE_UDP_PORT);
					socket.send(request);

					byte[] data_reply = new byte[300];
					DatagramPacket reply = new DatagramPacket(data_reply, data_reply.length);
					try {
						socket.receive(reply);
						msg = new String(reply.getData(), reply.getLength());
						if (msg.
							contains(String.format("%s_", CONNECTION_PREFIX))) {
							String[] msgArr = msg.split("_");
							if (!ipAddressMap.containsKey(msgArr[1]) && msgArr[1] != null) {
								ipAddressMap.put(msgArr[1], reply.getAddress());
								portsMap.
									put(msgArr[1], i + ClientUI.BASE_UDP_PORT);

							}
						}
					} catch (SocketTimeoutException ex) {
						Logger.getLogger(Variables.class.getName()).
							log(Level.SEVERE, null, ex);
					}
					socket.close();
				} catch (SocketException ex) {
					Logger.getLogger(Variables.class.getName()).
						log(Level.SEVERE, null, ex);
				} catch (IOException ex) {
					Logger.getLogger(Variables.class.getName()).
						log(Level.SEVERE, null, ex);
				}

			}
		}

	}
}

public class ClientUI extends javax.swing.JFrame {

	private String nickname;
	private static final int NICKNAME_MAX_LENGHT = 15;
	public static final int BASE_UDP_PORT = 28772;

	private static final String MESSAGRE_PREFIX = "MSG";

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	/**
	 * Creates new form ClientUI
	 */
	public ClientUI() {
		initComponents();
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        txtChatWindow = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtMessage = new javax.swing.JTextArea();
        btnSendMessage = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtServersConnected = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnSetupServers = new javax.swing.JButton();
        btnConnectServer = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("MultiChat Client");
        setBackground(new java.awt.Color(204, 204, 204));

        txtChatWindow.setEditable(false);
        txtChatWindow.setColumns(20);
        txtChatWindow.setRows(5);
        jScrollPane1.setViewportView(txtChatWindow);

        txtMessage.setColumns(20);
        txtMessage.setRows(3);
        jScrollPane2.setViewportView(txtMessage);

        btnSendMessage.setText("Send");
        btnSendMessage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendMessageActionPerformed(evt);
            }
        });

        txtServersConnected.setEditable(false);
        txtServersConnected.setColumns(20);
        txtServersConnected.setRows(5);
        jScrollPane3.setViewportView(txtServersConnected);

        jLabel1.setText("Chat Window");

        jLabel2.setText("Server List Connected");

        btnSetupServers.setText("Configure Servers");
        btnSetupServers.setToolTipText("");
        btnSetupServers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSetupServersActionPerformed(evt);
            }
        });

        btnConnectServer.setText("Connect Server");
        btnConnectServer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConnectServerActionPerformed(evt);
            }
        });

        jLabel3.setText("Message");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnSendMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel1)))
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnConnectServer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnSetupServers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jScrollPane3))
                                .addContainerGap(32, Short.MAX_VALUE))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConnectServer)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSetupServers)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSendMessage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE))
                .addGap(31, 31, 31))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSendMessageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendMessageActionPerformed
		// TODO add your handling code here:
		if (Variables.serversConnected.isEmpty()) {
			JOptionPane.
				showMessageDialog(this, "Chat is not connected to any server.", "Send Message", JOptionPane.WARNING_MESSAGE);
		} else {
			if (txtMessage.getText().isEmpty()) {
				JOptionPane.
					showMessageDialog(this, "Message field empty", "Send Message", JOptionPane.WARNING_MESSAGE);
			} else {
				List<Socket> sendMessage = new ArrayList();
				String message = String.
					format("(%s) %s", this.nickname, txtMessage.getText());
				this.writeMessageChatSended(message);
				for (Entry<Socket, Integer> entry : Variables.stateServers.
					entrySet()) {

					if (entry.getValue() == Variables.SEND_ONLY || entry.
						getValue() == Variables.SEND_RECIEVE) {
						sendMessage.add(entry.getKey());
					}

					DataOutputStream out;
					for (Socket socket : sendMessage) {
						try {
							out = new DataOutputStream(socket.getOutputStream());
							out.writeUTF(String.
								format("%s_%s", MESSAGRE_PREFIX, message));
						} catch (IOException ex) {
							Logger.getLogger(ClientUI.class.getName()).
								log(Level.SEVERE, null, ex);
						}
					}
				}
			}
			this.txtMessage.setText("");
			this.txtMessage.requestFocus();
		}
    }//GEN-LAST:event_btnSendMessageActionPerformed

    private void btnConnectServerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConnectServerActionPerformed
		// TODO add your handling code here:
		Map<String, InetAddress> ips = ServerSearchThread.ips;
		Map<String, Integer> ports = ServerSearchThread.ports;

		if (!ips.isEmpty()) {
			List<String> serverNames = new ArrayList(ips.keySet());
			JComboBox<String> cbxServerNames = new JComboBox(serverNames.
				toArray());
			cbxServerNames.setSelectedIndex(0);

			final String opt[] = {"OK", "Cancel"};
			final int YES = 0;
			int ins = JOptionPane.
				showOptionDialog(this, cbxServerNames, "Connect to Server", -1, JOptionPane.DEFAULT_OPTION, null, opt, opt[1]);
			if (ins == YES) {
				String serverName = (String) cbxServerNames.getSelectedItem();
				InetAddress ip = ips.get(serverName);
				List<String> nameServersOn = new ArrayList(Variables.serversConnected.
					values());
				try {
					if (!nameServersOn.contains(serverName)) {
						Socket socket = Variables.
							connectServer(serverName, ip, ports.get(serverName));
						if (Variables.setNickname(socket, nickname)) {
							Thread thr = new Thread(new RecieveServerThread(socket, serverName));
							thr.start();
							Variables.threadsList.put(serverName, thr);
							Variables.serversConnected.put(socket, serverName);
							Variables.stateServers.
								put(socket, Variables.SEND_RECIEVE);
							refreshServerList();
						} else {
							socket.close();
							JOptionPane.
								showMessageDialog(this, "Nickname already in use on this server.", "Connect Server", JOptionPane.WARNING_MESSAGE);
						}
					} else {
						JOptionPane.
							showMessageDialog(this, "Already connected to this server.", "Connect Server", JOptionPane.WARNING_MESSAGE);
					}
				} catch (IOException ex) {
					JOptionPane.showMessageDialog(this, "Error:\n" + ex.
												  getMessage(), "Connect Server", JOptionPane.ERROR_MESSAGE);
				}
			}

		} else {
			JOptionPane.
				showMessageDialog(this, "No Available Servers.", "Connect Server", JOptionPane.WARNING_MESSAGE);
		}
    }//GEN-LAST:event_btnConnectServerActionPerformed

    private void btnSetupServersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSetupServersActionPerformed
		ServerConfig dialog = new ServerConfig(this, true, this.nickname);
		dialog.showDialog();
		this.refreshServerList();
    }//GEN-LAST:event_btnSetupServersActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		Variables.chat = new ClientUI();
		if (Variables.chat.insertNickname()) {
			Variables.chat.uploadServersConfigFile();

			/* Create and display the form */
			java.awt.EventQueue.invokeLater(new Runnable() {
				public void run() {
					Variables.chat.setVisible(true);
				}
			});
		}

	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConnectServer;
    private javax.swing.JButton btnSendMessage;
    private javax.swing.JButton btnSetupServers;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea txtChatWindow;
    private javax.swing.JTextArea txtMessage;
    private javax.swing.JTextArea txtServersConnected;
    // End of variables declaration//GEN-END:variables

	/**
	 * Importa Lista com Servidores a Pesquisar neste Cliente
	 */
	private void uploadServersConfigFile() {
		String[] optYesNo = {"Yes", "No"};
		String title = "Upload Servers Configuration File";
		final int YES = 0;
		boolean configFileAdded = false;

		do {
			int ans = JOptionPane.
				showOptionDialog(this, "Load Servers Configuration File?", title, 0, JOptionPane.QUESTION_MESSAGE, null, optYesNo, optYesNo[1]);

			if (ans == YES) {
				JFileChooser chDialog = new JFileChooser();
				chDialog.setDialogTitle("Import Server Configuration File");
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files (.txt)", "txt");
				chDialog.setFileFilter(filter);
				int returnVal = chDialog.showOpenDialog(this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					try {
						readServers(chDialog.getSelectedFile());
						Variables.startServerSearch();
						configFileAdded = true;
					} catch (Exception ex) {
						JOptionPane.
							showMessageDialog(this, "Can't open/read the provided file.", title, JOptionPane.WARNING_MESSAGE);
					}
				}
			} else {
				configFileAdded = true;
			}
		} while (!configFileAdded);
		Variables.startServerSearch();
	}

	/**
	 * Apresenta JOptionPane para pedir por Nickname
	 */
	private boolean insertNickname() {
		boolean nicknameCorrect = false;
		String nick;
		do {
			nick = JOptionPane.
				showInputDialog(null, "Insert your Nickname:", "Multichat - Nickname", JOptionPane.PLAIN_MESSAGE);
			if (nick != null) {
				nicknameCorrect = setNickname(nick);
				if (!nicknameCorrect) {
					JOptionPane.
						showMessageDialog(null, "Invalid Nickname!\nCheck if nickname is lower than " + NICKNAME_MAX_LENGHT + " characters.", "Multichat - Nickname", JOptionPane.WARNING_MESSAGE);
				}
			} else {
				close();
				return nicknameCorrect;
			}
		} while (!nicknameCorrect);

		return nicknameCorrect;
	}

	/**
	 * Valida Nickname Inserido
	 *
	 * @param nick nickname inserido
	 * @return true se válido (e atribuido), false se invalido
	 */
	private boolean setNickname(String nick) {
		if (nick.length() <= NICKNAME_MAX_LENGHT && nick.length() > 0) {
			this.nickname = nick;
			return true;
		}

		return false;
	}

	/**
	 * Apresenta Diálogo de Confirmação de Saída
	 */
	private void close() {
		String[] optYesNo = {"Yes", "No"};
		final int YES = 0;
		int ans = JOptionPane.
			showOptionDialog(this, "Do you really want to quit the Multichat?", "Quit Multichat", 0, JOptionPane.QUESTION_MESSAGE, null, optYesNo, optYesNo[1]);

		if (ans == YES) {
			dispose();
			Variables.disconnect(this.nickname);
		}
	}

	private void readServers(File file) throws FileNotFoundException, IOException {
		List<String> servers = new ArrayList<>();
		BufferedReader in = new BufferedReader(new FileReader(file));

		String line;
		while ((line = in.readLine()) != null) {
			servers.add(line);
		}
		in.close();

		InetAddress ipServer;
		List<InetAddress> availableServers = new ArrayList<>();
		for (String server : servers) {
			try {
				ipServer = InetAddress.getByName(server);
				availableServers.add(ipServer);
			} catch (UnknownHostException ex) {
				JOptionPane.showMessageDialog(this, String.
											  format("Can't connect to server %s.", server), "Upload Servers Configuration File", JOptionPane.WARNING_MESSAGE);
			}
		}
		Variables.
			connectServers(availableServers, Variables.ipServers, Variables.portsServers);
	}

	public void writeMessageChatAreaRecieved(String message,
											 String server, String ipAddress,
											 int udp_port) {
		this.txtChatWindow.append(String.
			format("<< %s\n<%s@%s:%d> %s\n", message, server, ipAddress, udp_port, dateFormat.
				   format(new Date())));
	}

	public void writeMessageChatSended(String message) {
		this.txtChatWindow.append(String.
			format(">> %s\n%s\n", message, dateFormat.format(new Date())));
	}

	public void refreshServerList() {
		this.txtServersConnected.setText("");
		for (Entry<Socket, String> entry : Variables.serversConnected.entrySet()) {
			this.txtServersConnected.append(String.format("%s <%s>\n", entry.
														  getValue(), entry.
														  getKey().
														  getInetAddress()));
		}
	}
}

/**
 * Class UDP Broadcast Thread
 *
 * @author dnamorim
 */
class UDPBroadcastThread implements Runnable {

	private int port;
	private Map<String, InetAddress> ipMap;
	private Map<String, Integer> portsMap;

	public UDPBroadcastThread(int udp_port, Map<String, InetAddress> ipList,
							  Map<String, Integer> udpPortsList) {
		this.port = udp_port;
		this.ipMap = ipList;
		this.portsMap = udpPortsList;
	}

	@Override
	public void run() {
		try {
			DatagramSocket socket = new DatagramSocket();
			socket.setSoTimeout(Variables.TIMEOUT_SOCKET);
			socket.setBroadcast(true);
			InetAddress ipDestination = InetAddress.
				getByName("255.255.255.255");

			String msg = Variables.CONNECTION_PREFIX;
			byte[] data_receiver = new byte[300];
			byte[] data = msg.getBytes();

			DatagramPacket request = new DatagramPacket(data, msg.length(), ipDestination, this.port);

			socket.send(request);

			DatagramPacket reply = new DatagramPacket(data_receiver, data_receiver.length);

			try {
				socket.receive(reply);
				msg = new String(reply.getData(), 0, reply.getLength());
				ipDestination = reply.getAddress();

				if (msg.contains(Variables.CONNECTION_PREFIX)) {
					String[] parts = msg.split("_");

					if (!ipMap.containsKey(parts[1])) {
						if (parts[1] != null) {
							ipMap.put(parts[1], ipDestination);
							portsMap.put(parts[1], port);
						}
					}
				}
			} catch (SocketTimeoutException ex) {

			}
			socket.close();
		} catch (SocketException ex) {

		} catch (IOException ex) {

		}
	}
}

/**
 * END Class UDP Broadcast Thread
 */
/**
 * Class Server Search Thread
 *
 * @author dnamorim
 */
class ServerSearchThread implements Runnable {

	private final int port;
	public static Map<String, InetAddress> ips = Variables.ipServers;
	public static Map<String, Integer> ports = Variables.portsServers;

	public ServerSearchThread(int port) {
		this.port = port;
	}

	@Override
	public void run() {
		int j = 0;
		while (true) {
			for (int i = 0; i < 20; i += 2) {
				try {
					Thread t = new Thread(new UDPBroadcastThread(port + i, ips, ports));
					t.start();
					t.join();
				} catch (InterruptedException ex) {
					Logger.getLogger(ServerSearchThread.class.getName()).
						log(Level.SEVERE, null, ex);
				}
			}
			j++;
			if (j == 1000) {
				ips.clear();
				ports.clear();
			}
		}
	}
}

/**
 * END Server Search Thread
 */
/**
 * Class Recieve Server Thread
 *
 * @author dnamorim
 */
class RecieveServerThread extends Thread implements Runnable {

	private final Socket socket;
	private DataInputStream in;
	private final String serverName;
	private static final int TIMEOUT_SOCKET = 3;
	private static final String DISCONNECT_MESSAGE = "OFF";

	public RecieveServerThread(Socket client_socket, String name) {
		this.socket = client_socket;
		this.serverName = name;
	}

	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			if (socket.isClosed()) {
				break;
			}
			if (socket.isInputShutdown()) {
				break;
			}
			if (this.isInterrupted()) {
				break;
			}
			try {
				socket.setSoTimeout(TIMEOUT_SOCKET);
				in = new DataInputStream(socket.getInputStream());
				String message;
				message = in.readUTF();
				if (message != null) {
					if (message.compareToIgnoreCase(DISCONNECT_MESSAGE) == 0) {
						Variables.deleteServer(socket);
						this.interrupt();
					} else {
						// WRITE MESSAGE
						if (Variables.stateServers.get(socket) != Variables.SEND_ONLY) {
							Variables.chat.
								writeMessageChatAreaRecieved(message, this.serverName, socket.
															 getInetAddress().
															 getHostAddress(), socket.
															 getPort());
						}
					}
				}

			} catch (IOException ex) {

			}

		}
	}
}
/**
 * END Class Recieve Sever Thread
 */
