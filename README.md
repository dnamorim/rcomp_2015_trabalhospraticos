# rcomp_2015_trabalhospraticos

Repositório dos Trabalhos Práticos da Unidade Curricular "Redes de Computadores" (RCOMP) do 2º Ano da [Licenciatura de Engenharia Informática](http://www.isep.ipp.pt/Course/Course/26) do [Instituto Superior de Engenharia do Porto](http://www.isep.ipp.pt) (LEI-ISEP).

© [Duarte Amorim](https://github.com/dnamorim), 2015